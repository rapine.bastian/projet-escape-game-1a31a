import java.sql.*;

public class ConnectionMySQL {

    private Connection mysql;
    private boolean connecte = false;

    /** Charge le driver */
    public ConnectionMySQL() throws ClassNotFoundException{
        try{
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("Driver MySQL trouvé");
        }
        catch(ClassNotFoundException e){
            System.out.println("Driver MySQL non trouvé fefe zfezes");
            mysql = null;
            return;
        }
    }

    /** crée la connexion */
    public void connecter() throws SQLException {
        try{
            mysql = DriverManager.getConnection("jdbc:mysql://" + "localhost" + ":3306/" + "echappee_belle","lopez","lopez");
            System.out.println("Connexion reussie");
            connecte = true;
        }
        catch(SQLException e){
            System.out.println("Echec de connection");
            System.out.println(e.getMessage());
            mysql = null;
            return;
        }
    }

    /** ferme la connexion */
    public void close() throws SQLException {

    }

    public boolean isConnecte(){
        return this.connecte;
    }

    public Blob createBlob()throws SQLException{
        return this.mysql.createBlob();
    }

    public Statement createStatement() throws SQLException {
        return this.mysql.createStatement();
    }

    public PreparedStatement prepareStatement(String requete) throws SQLException{
        return this.mysql.prepareStatement(requete);
    }
}
