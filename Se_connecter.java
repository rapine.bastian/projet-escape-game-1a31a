import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Se_connecter {

    private Stage st;
    private Scene sceneConnecter;
    private ControlleurScene control;
    private TextField tPseudo;
    private PasswordField tmdp;



    public  Se_connecter(ControlleurScene control) {
        this.control = control;
        this.tmdp = new PasswordField();
        this.tPseudo = new TextField();
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(15,20,10,10));
        root.setTop(creerHeader());
        root.setBottom(creerFooter());
        root.setCenter(creerFormulaire());
        this.sceneConnecter = new Scene(root, 500, 500);


    }

    public PasswordField getMdp(){
        return this.tmdp;
    }

    public TextField getPseudo(){
        return this.tPseudo;
    }

    public Scene getSceneConnecter(){
        return this.sceneConnecter;
    }


    public GridPane creerFormulaire() {

        GridPane grid = new GridPane();
        grid.setVgap(10);


        Label lPseudo = new Label("Pseudo :");


        Label lSouvenir = new Label("Se souvenir de moi ? ");
        CheckBox CSouvenir = new CheckBox();
        CSouvenir.setIndeterminate(false);


        Label lMdp = new Label("Mot de passe :");


        Button connexion = new Button("Connexion ↓ ");
        connexion.setOnAction(new LesBoutons(this.control));
        connexion.setStyle("-fx-background-color: #A454A6");
        connexion.setPrefWidth(200);
        connexion.setPrefHeight(25);

        // Image
        ImageView img = new ImageView("./Logo.png");
        img.setFitWidth(125);
        img.setPreserveRatio(true);

        grid.add(img, 6, 1);
        grid.add(lPseudo, 2,2);
        grid.add(this.tPseudo, 3,2);
        grid.add(lMdp, 2,3);
        grid.add(this.tmdp, 3,3);
        grid.add(lSouvenir, 2, 4);
        grid.add(CSouvenir,3,4);
        grid.add(connexion, 3,5);
        grid.setMargin(lPseudo, new Insets(0,5,0,20));
        grid.setMargin(lMdp, new Insets(0,5,0,20));
        grid.setMargin(lSouvenir, new Insets(0,5,0,20));
        grid.setStyle("-fx-background-color: #ece3d7;");

        return grid;
    }

    public BorderPane creerFooter() {

        BorderPane grid = new BorderPane();


        Hyperlink creerCompte = new Hyperlink("Créer un compte");
        creerCompte.setOnAction(new LesHyperlinks(this.control));


        creerCompte.setFont(new Font("Roboto", 15));
        creerCompte.setUnderline(true);
        grid.setCenter(creerCompte);
        grid.setStyle("-fx-background-color: #ece3d7;");

        return grid;
    }

    public GridPane creerHeader() {

        GridPane grid = new GridPane();
        Label titre = new Label("Se connecter");
        grid.add(titre, 2,0);
        titre.setFont(new Font("Saira", 40));
        titre.setStyle("-fx-font-weight: bold");
        grid.setPadding(new Insets(5,0,5,0));
        grid.setAlignment(Pos.CENTER);
        grid.setStyle("-fx-background-color: #ece3d7;");
        return grid;
    }

}