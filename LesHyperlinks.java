import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;


import java.sql.SQLException;

public class LesHyperlinks implements EventHandler<ActionEvent> {

    private ControlleurScene control;
    private Se_connecter se_connecter;

    public LesHyperlinks(ControlleurScene control) {
        this.control = control;
        this.se_connecter = this.control.getSeConnecter();
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Hyperlink h = (Hyperlink) actionEvent.getTarget();



        if (h.getText().equals("Créer un compte")) {
            control.changerScene(control.getSceneCreerCompte());
        } else if (h.getText().equals("Vous avez déjà un compte ?")) {
            control.changerScene(control.getSceneConnecter());}

        if (h.getText().equals("Lancez vous ↓ ")) {
            try {
                boolean tCU = control.getConnexionBD().creation(control.getCreerCompte().getPseudo().getText(), control.getCreerCompte().getMdp().getText(), control.getCreerCompte().getEmail().getText());
                if (tCU==true){
                    System.out.println("création du compte reussie");
                } else{System.out.println("echec creation du compte");}
            } catch (SQLException throwables) {
                System.out.println("mmmmmmmmm");
                throwables.printStackTrace();
            }
        }

        if (h.getText().equals("Rechercher")){
            String pseudo = this.control.getAdminRechercher().getTfNomJoueur().getText();
            try {
                this.control.changerScene(new AdminFicheJoueur(this.control, pseudo).getScnenAdminFicheJoueur());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    }
}