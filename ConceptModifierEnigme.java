import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ConceptModifierEnigme extends BorderPane {


    /**
     * Label clickable permettant le retour à la scene Concepteur
     */
    private final Label lRetour;

    /**
     * Label affichant le nom de l'utilisateur connecte
     */
    private Label lPseudoConcept;


    /**
     * TextField donnant le nom de l'enigme
     */
    private TextField tfNomEnigme;

    /**
     * TextArea donnant le texte de la question
     */
    private TextArea taTexteQuestion;

    /**
     * TextArea donnant le texte de la reponse
     */
    private TextArea taTexteReponse;

    /**
     * TextArea donnant le texte de l'indice
     */
    private TextArea taTexteIndice;

    /**
     * Button ouvrant un FileChooser afin de selectionner une image pour l'enigme
     */
    private Button btImage;

    /**
     * Image illustrant l'énigme
     */
    private Image image;

    /**
     * ImageView permettant d'affiche l'image
     */
    private ImageView miniature;

    /**
     * Button permettant de sauvegarder l'enigme
     */
    private final Button btSauvegarder;

    private Scene sceneConceptModifEnigme;
    private ControlleurScene control;

    /**
     * Permet de creer la partie centrale (voir constructeur)
     * @param pseudo
     * @param nomEnigme
     * @param texteQuestion
     * @param texteReponse
     * @param texteIndice
     * @param imageSelect
     * @return BorderPane a mettre dans la partie centrale
     */
    private BorderPane center(String pseudo, String nomEnigme, String texteQuestion, String texteReponse, String texteIndice){

        Label lPseudo = new Label("Pseudo : ");
        lPseudo.setFont(Font.font("Inter",16));
        lPseudo.setTextFill(Color.BLACK);

        this.lPseudoConcept = new Label(pseudo);
        lPseudoConcept.setFont(Font.font("Inter", FontWeight.BOLD,16));
        lPseudoConcept.setTextFill(Color.BLACK);

        HBox centerTopCenter = new HBox();
        centerTopCenter.setAlignment(Pos.CENTER_RIGHT);
        centerTopCenter.setPadding(new Insets(8));
        centerTopCenter.getChildren().addAll(lPseudo,this.lPseudoConcept);

        BorderPane topCenter = new BorderPane();
        topCenter.setPadding(new Insets(8));
        topCenter.setCenter(centerTopCenter);



        Label lNomEnigme = new Label("Nom de l'énigme :");
        lNomEnigme.setFont(Font.font("Inter",16));
        lNomEnigme.setTextFill(Color.BLACK);

        this.tfNomEnigme = new TextField(nomEnigme);

        Label lTexteQuestion = new Label("La Question :");
        lTexteQuestion.setFont(Font.font("Inter",16));
        lTexteQuestion.setTextFill(Color.BLACK);
        lTexteQuestion.setUnderline(true);

        this.taTexteQuestion = new TextArea(texteQuestion);
        this.taTexteQuestion.setMaxSize(500,100);

        Label lTexteReponse = new Label("La Réponse :");
        lTexteReponse.setFont(Font.font("Inter",16));
        lTexteReponse.setTextFill(Color.BLACK);
        lTexteReponse.setUnderline(true);

        this.taTexteReponse = new TextArea(texteReponse);
        this.taTexteReponse.setMaxSize(500,100);

        Label lTexteIndice = new Label("Indice(s) :");
        lTexteIndice.setFont(Font.font("Inter",16));
        lTexteIndice.setTextFill(Color.BLACK);
        lTexteIndice.setUnderline(true);

        this.taTexteIndice = new TextArea(texteIndice);
        this.taTexteIndice.setMaxSize(500,100);

        this.btImage = new Button("Choisir une image");
/*
        if (imageSelect == null) {
            this.image = new Image("file:img/NoImage.png");
        }
        else {
            this.image = imageSelect;
        }
        /*
 */
        this.miniature = new ImageView(this.image);
        this.miniature.setFitHeight(100);
        this.miniature.setPreserveRatio(true);

        GridPane centerCenterCenter = new GridPane();
        centerCenterCenter.setPadding(new Insets(8));
        centerCenterCenter.setHgap(8);
        centerCenterCenter.setVgap(8);
        centerCenterCenter.add(lNomEnigme,0,0);
        centerCenterCenter.add(this.tfNomEnigme,1,0);
        centerCenterCenter.add(lTexteQuestion,0,2,2,1);
        centerCenterCenter.add(this.taTexteQuestion,0,3,2,1);
        centerCenterCenter.add(lTexteReponse,2,2,2,1);
        centerCenterCenter.add(this.taTexteReponse,2,3,2,1);
        centerCenterCenter.add(lTexteIndice,0,4,2,1);
        centerCenterCenter.add(this.taTexteIndice,0,5,2,1);
        centerCenterCenter.add(this.btImage,2,5);
        centerCenterCenter.add(this.miniature,3,5);

        BorderPane centerCenter = new BorderPane();
        centerCenter.setPadding(new Insets(8));
        centerCenter.setCenter(centerCenterCenter);


        BorderPane center = new BorderPane();
        center.setStyle("-fx-background-color: #76F8E0;");
        center.setTop(topCenter);
        center.setCenter(centerCenter);
        return center;
    }


    public ConceptModifierEnigme(ControlleurScene control, String pseudo, String nomEnigme, String texteQuestion, String texteReponse, String texteIndice){ //Image imageSelect
        this.control = control;

        FlowPane top = new FlowPane();
        top.setPadding(new Insets(8));
        top.setStyle("-fx-background-color: #1FAFAF");
        top.setAlignment(Pos.CENTER_LEFT);
        Label lTop = new Label("Modifier une énigme");
        lTop.setFont(Font.font("Inter", FontWeight.BOLD,32));
        lTop.setTextFill(Color.BLACK);
        top.getChildren().add(lTop);
        this.setTop(top);


        this.setCenter(this.center(pseudo, nomEnigme, texteQuestion, texteReponse, texteIndice));


        FlowPane leftBottom = new FlowPane();
        leftBottom.setPadding(new Insets(8));
        leftBottom.setAlignment((Pos.CENTER_LEFT));
        this.lRetour = new Label("Retour");
        this.lRetour.setOnMouseClicked(new LesLabels(this.control, "BDenigmes"));
        this.lRetour.setUnderline(true);
        this.lRetour.setFont(Font.font("Inter", FontWeight.BOLD,16));
        this.lRetour.setTextFill(Color.web("87009D"));
        leftBottom.getChildren().add(this.lRetour);

        FlowPane centerBottom = new FlowPane();
        centerBottom.setPadding(new Insets(8));
        centerBottom.setAlignment((Pos.CENTER_LEFT));
        this.btSauvegarder = new Button("Sauvegarder");
        this.btSauvegarder.setFont(Font.font("Inter",32));
        centerBottom.getChildren().add(this.btSauvegarder);

        BorderPane bottom = new BorderPane();
        bottom.setPadding(new Insets(8));
        bottom.setStyle("-fx-background-color: #76F8E0");
        bottom.setLeft(leftBottom);
        bottom.setCenter(centerBottom);

        this.setBottom(bottom);

        this.sceneConceptModifEnigme = new Scene(this, 1050, 700d);
    }

    public Scene getSceneConceptModifEnigme(){
        return this.sceneConceptModifEnigme;
    }
}