import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ScenarioBD{
    private ConnectionMySQL laConnexion;

    public ScenarioBD(ConnectionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }

    public List<Scenario> getScenario() throws SQLException{
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select titresc, brouillonsc, messageTxt from SCENARIO natural join TEXTE order by titresc");
        List<Scenario> res = new ArrayList<>();
        while (rs.next()){
            String nomca = rs.getString(1);
            boolean etat = rs.getString(2).charAt(0) == 'O'; // true si c'est un brouillon
            String intro = rs.getString(3);
            rs.next();
            String succes = rs.getString(3);
            rs.next();
            String echec = rs.getString(3);
            res.add(new Scenario(nomca, etat,intro, succes, echec));
        }
        return res;
    }


    public Scenario getScenarioParNom(String nomScenario) throws SQLException {
        boolean res;
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select titresc, brouillonsc, messageTxt  from SCENARIO natural join TEXTE where titresc = '" + nomScenario+"' order by titresc");
        if (rs.next()) {
            String nomca = rs.getString(1);
            boolean etat = rs.getString(2).charAt(0) == 'O'; // true si c'est un brouillon
            String intro = rs.getString(3);
            rs.next();
            String succes = rs.getString(3);
            rs.next();
            String echec = rs.getString(3);
           return new Scenario(nomca, etat,intro, succes, echec);
        } else {
            throw new SQLException("Le scénario " + nomScenario + " n'existe pas.");
        }
    }


     public void effacerScenario(int num) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("delete from TEXTE, PARTICIPER, SCENARIO where idsc = ?");
        pst.setInt(1, num);
        pst.executeUpdate();
        PreparedStatement pst2 = this.laConnexion.prepareStatement("delete from SCENARIO where idsc = ?");
        pst2.setInt(1, num);
        pst2.executeUpdate();
    }
/*
    public void insererScenario( Scenario scn, byte[] icone, byte[] imgSucces, byte[] imgEchec, byte[] imgIntro) throws SQLException{
        PreparedStatement pst = this.laConnexion.prepareStatement("insert into SCENARIO values(?,?,?,?,?,?,?)");`
        ResultSet rs = st.executeQuery("select IFNULL(max(idsc),0) lemax from SCENARIO");
        rs.next();
        int res = rs.getInt("lemax"); rs.close();
        pst.setInt(1, res+1);
        pst.setString(2, scn.getTitre());
        pst.setString(3, scn.getResume());

        Blob b=laConnexion.createBlob (); //image
        b.setBytes(1, icone);
        pst.setBlob(7, b);

        pst.setDate(5, scn.getTpsMax());
        pst.setDate(6, scn.getDateMiseEnLigne());
        pst.setBoolean(7, (scn.getEtat()));
        pst.setInt(8, scn.getNumConcepteur()); // peut etre changer car on peut avoir le num ailleurs
        pst.executeUpdate();

        PreparedStatement pst2 = this.laConnexion.prepareStatement("insert into TEXTE values(?,?,?,?,?,?)"); //insertion texte intro
        ResultSet rs2 = st.executeQuery("select IFNULL(max(idtxt),0) lemax from TEXTE");
        rs2.next();
        int res2 = rs2.getInt("lemax"); rs2.close();
        pst2.setInt(1, res2 +1);
        pst2.setString(2, scn.getMessageTxtIntro());
        pst2.setString(3, scn.getImgTxtIntro());

        Blob b2=laConnexion.createBlob (); //image
        b2.setBytes(1, imgIntro);
        pst2.setBlob(4, b2);

        pst2.setInt(5, res+1);
        pst2.setInt(6, 1);

        PreparedStatement pst3 = this.laConnexion.prepareStatement("insert into TEXTE values(?,?,?,?,?,?)"); //insertion texte en cas de succés
        pst3.setInt(1, res2 +2);
        pst3.setString(2, scn.getMessageTxtSucces());
        pst3.setString(3, scn.getImgTxtSucces());

        Blob b3=laConnexion.createBlob (); //image
        b3.setBytes(1, imgSucces);
        pst3.setBlob(4, b3);

        pst3.setInt(5, res+1);
        pst3.setInt(6, 2);

        PreparedStatement pst4 = this.laConnexion.prepareStatement("insert into TEXTE values(?,?,?,?,?,?)"); //insertion texte en cas d'échec
        pst4.setInt(1, res2 +3);
        pst4.setString(2, scn.getMessageTxtEchec());
        pst4.setString(3, scn.getImgTxtEchec());
        
        Blob b4=laConnexion.createBlob (); //image
        b4.setBytes(1, imgEchec);
        pst4.setBlob(4, b4);

        pst4.setInt(5, res+1);
        pst4.setInt(6, 3);

        int i = 0;
        for (Carte c : scn.getListeCarte()){
            i += 1;
            PreparedStatement pst5 = this.laConnexion.prepareStatement("insert into PARTICIPER values(?,?,?)"); //insertion table PARTICIPER (pour lier carte a un scenario)
            pst5.setInt(1, c.getNumCarte());
            pst5.setInt(2, scn.getNumScenario());
            pst5.setInt(3, i);
        }
    }
    /
 */
}
