import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;


import java.sql.SQLException;

public class LesBoutons implements EventHandler<ActionEvent> {

    private ControlleurScene control;
    private Se_connecter se_connecter;

    public LesBoutons(ControlleurScene control) {
        this.control = control;
        System.out.println(this.control);
        this.se_connecter = this.control.getSeConnecter();
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Button b = (Button) actionEvent.getTarget();


        if (b.getText().equals("Connexion ↓ ")) {
            try {
                String role = control.getConnexionBD().connexion(control.getSeConnecter().getPseudo().getText(), control.getSeConnecter().getMdp().getText());
                if (role.equals("Admin")){
                    control.creerSceneUneFoisConnecteAdmin();
                    control.changerScene(control.getAdminRechercher().getSceneAdminRechercher());
                }

                else {
                    control.creerSceneUneFoisConnecteConcepteur();
                    control.changerScene(control.getMenuConcepteur().getSceneMenuConcepteur());

                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        if (b.getText().equals("Carte")) {
            control.changerScene(control.getConceptCreationCarte().getSceneConceptCreationCarte());
        }
        if (b.getText().equals("Scenario")) {
            control.changerScene(control.getConceptCreationScenario().getSceneConceptCreationScenar());
        }
        if (b.getText().equals("Enigme")) {
            control.changerScene(control.getConceptCreationEnigme().getSceneConceptCreationEnigme());
        }


        if (b.getText().equals("BDEnigme")) {
            control.changerScene(control.getBdEnigme().getBdEnigmeScene());
        }
        if (b.getText().equals("BDScenario")) {
            control.changerScene(control.getBdScenario().getBdScenarioScene());
        }
        if (b.getText().equals("BDCarte")) {
            control.changerScene(control.getBdCarte().getBdCarteScene());
        }




    }
}