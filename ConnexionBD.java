import java.sql.*;

public class ConnexionBD{

    private ConnectionMySQL laconnexion;
    private String pseudo = "Exemple";

    public ConnexionBD(ConnectionMySQL laconnexion){
        this.laconnexion = laconnexion;
    }

    public String connexion(String pseudo, String mdp) throws SQLException{
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select nomrole from ROLE natural join UTILISATEUR where pseudout = '" + pseudo + "' and mdput = password('" + mdp +"')");
        if (rs.next()){
            if (rs.getString(1).charAt(0) == 'J'){
                throw new SQLException("Le module joueur n'est pas implémenté.");
            }
            this.pseudo = pseudo;
            return rs.getString(1);
        }
        else{
            throw new SQLException("Cet utilisateur n'existe pas !");
        }
    }

    public String getPseudo() {
        return pseudo;
    }

    public boolean creation(String pseudo, String mdp, String email) throws SQLException{ //byte[] avatar
        PreparedStatement pst = this.laconnexion.prepareStatement("insert into UTILISATEUR values(?,?,?,?,?,?,?)");
        Statement st = this.laconnexion.createStatement();

        ResultSet rs1 = st.executeQuery("select pseudout, emailut, mdput  from UTILISATEUR where pseudout = '" + pseudo+"' or emailut = '"+email+"'");
        while (rs1.next()){
            if (rs1.getString(1).equals(pseudo) || rs1.getString(1).length() == 0 || rs1.getString(2).length() == 0 || rs1.getString(3).length() == 0 ||  rs1.getString(2).equals(email)){
                return false;
            }
        }
        rs1.close();
        ResultSet rs = st.executeQuery("select IFNULL(max(idUt),0) lemax from UTILISATEUR");
        rs.next();
        int res = rs.getInt("lemax");
        System.out.println(res);
        rs.close();
        pst.setInt(1, res+1);
        pst.setString(2, pseudo);
        pst.setString(3, email);
        pst.setString(4, mdp);
        pst.setString(5, "N");
        ///Blob b = this.laconnexion.createBlob();
        ///b.setBytes(1, avatar);
        ///pst.setBlob(6, b);
        pst.setInt(7, 2);
        pst.executeUpdate();
        return true;
    }

}
