import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Creer_compte {

    private  ControlleurScene control;
    private Scene sceneCreerCompte;
    private TextField tPseudo, tEmail;
    private PasswordField tmdp;

    public Creer_compte(ControlleurScene control) {
        this.control = control;
        this.tmdp = new PasswordField();
        this.tPseudo = new TextField();
        this.tEmail = new TextField();
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(15,20,10,10));
        root.setTop(creerHeader());
        root.setBottom(creerFooter());
        root.setCenter(creerFormulaire());
        this.sceneCreerCompte = new Scene(root, 500 , 500);
    }

    public PasswordField getMdp(){
        return this.tmdp;
    }

    public TextField getPseudo(){
        return this.tPseudo;
    }

    public Scene getSceneCreerCompteCreerCompte(){
        return this.sceneCreerCompte;
    }

    public TextField getEmail(){
        return this.tEmail;
    }

    public GridPane creerFormulaire() {

        GridPane grid = new GridPane();
        grid.setVgap(10);


        Label lPseudo = new Label("Pseudo :");

        tPseudo.setPrefWidth(200);
        tPseudo.setPrefHeight(25);

        Label lMdp = new Label("Mot de passe :");

        Label lEmail = new Label("Email :");
        lEmail.setFont(new Font(20));
        lEmail.setStyle("-fx-font-weight: bold");

        Hyperlink connexion = new Hyperlink("Lancez vous ↓ ");
        connexion.setOnAction(new LesHyperlinks(this.control));
        connexion.setStyle("-fx-background-color: #A454A6");
        connexion.setPrefWidth(200);
        connexion.setPrefHeight(25);

        grid.add(lPseudo, 2,2);
        grid.add(tPseudo, 3,2);
        grid.add(lMdp, 2,3);
        grid.add(tmdp, 3,3);
        grid.add(connexion, 3,5);
        grid.add(lEmail,2,4);
        grid.add(tEmail,3,4);
        grid.setMargin(lEmail, new Insets(0,5,0,20));
        grid.setMargin(lPseudo, new Insets(0,5,0,20));
        grid.setMargin(lMdp, new Insets(0,5,0,20));
        grid.setStyle("-fx-background-color: #ece3d7;");

        return grid;
    }

    public BorderPane creerFooter() {

        BorderPane grid = new BorderPane();


        Hyperlink numFiche = new Hyperlink("Vous avez déjà un compte ?");
        numFiche.setOnAction(new LesHyperlinks( control));


        numFiche.setFont(new Font("Roboto", 15));
        numFiche.setUnderline(true);
        grid.setCenter(numFiche);
        grid.setStyle("-fx-background-color: #ece3d7;");

        return grid;
    }

    public GridPane creerHeader() {

        GridPane grid = new GridPane();


        Label titre = new Label("Créer votre compte");

        grid.add(titre, 2,0);
        titre.setFont(new Font("Saira", 40));
        titre.setStyle("-fx-font-weight: bold");
        grid.setPadding(new Insets(5,0,5,0));
        grid.setAlignment(Pos.CENTER);
        grid.setStyle("-fx-background-color: #ece3d7;");

        return grid;
    }

}