import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ConceptModifierCarte extends BorderPane {


    /**
     * Label clickable permettant le retour à la scene Concepteur
     */
    private final Label lRetour;

    /**
     * Label affichant le nom de l'utilisateur connecte
     */
    private Label lPseudoConcept;


    /**
     * TextArea donnant le nom du scenario
     */
    private TextField tfNomCarte;

    /**
     * TextArea donnant le texte introductif
     */
    private TextArea taTexteIntroductif;

    /**
     * TextArea donnant le texte en cas de succes
     */
    private TextArea taTexteSucces;

    /**
     * TextArea donnant le texte en cas d'echec
     */
    private TextArea taTexteEchec;

    /**
     * Button ouvrant un FileChooser afin de selectionner une image pour le texte introductif
     */
    private Button btImageIntroductif;

    /**
     * Button ouvrant un FileChooser afin de selectionner une image pour le texte en cas de succes
     */
    private Button btImageSucces;

    /**
     * Button ouvrant un FileChooser afin de selectionner une image pour le texte en cas d'echec
     */
    private Button btImageEchec;

    /**
     * Image illustrant le texte d'introduction
     */
    private Image imageIntroduction;

    /**
     * ImageView permettant d'affiche l'image d'introduction
     */
    private ImageView miniatureIntroduction;

    /**
     * Image illustrant le texte de succes
     */
    private Image imageSucces;

    /**
     * ImageView permettant d'affiche l'image de succes
     */
    private ImageView miniatureSucces;

    /**
     * Image illustrant le texte d'echec
     */
    private Image imageEchec;

    /**
     * ImageView permettant d'affiche l'image d'echec
     */
    private ImageView miniatureEchec;

    /**
     * Button permettant de changer la scene vers la modification du tileset
     */
    private Button btAssocierTileSet;

    /**
     * Groupe de bouton permettant de savoir si la carte est prete ou non
     */
    private ToggleGroup tgEtat;

    /**
     * Button permettant de sauvegarder la carte
     */
    private final Button btSauvegarder;

    private Scene sceneConceptModifCarte;
    private ControlleurScene control;


    private BorderPane center(String pseudo, String nomCarte, String etat,
                              String texteIntroductif, String texteSucces, String texteEchec,
                              Image imageSelectIntroductif, Image imageSelectSucces, Image imageSelectEchec){

        Label lPseudo = new Label("Pseudo : ");
        lPseudo.setFont(Font.font("Inter",16));
        lPseudo.setTextFill(Color.BLACK);

        this.lPseudoConcept = new Label(pseudo);
        lPseudoConcept.setFont(Font.font("Inter", FontWeight.BOLD,16));
        lPseudoConcept.setTextFill(Color.BLACK);

        HBox centerTopCenter = new HBox();
        centerTopCenter.setAlignment(Pos.CENTER_RIGHT);
        centerTopCenter.setPadding(new Insets(8));
        centerTopCenter.getChildren().addAll(lPseudo,this.lPseudoConcept);

        BorderPane topCenter = new BorderPane();
        topCenter.setPadding(new Insets(8));
        topCenter.setCenter(centerTopCenter);



        Label lNomCarte = new Label("Nom :");
        lNomCarte.setFont(Font.font("Inter",16));
        lNomCarte.setTextFill(Color.BLACK);

        this.tfNomCarte = new TextField(nomCarte);

        Label lEtat = new Label("Etat :");
        lEtat.setFont(Font.font("Inter",16));

        this.tgEtat = new ToggleGroup();
        RadioButton tbBrouillon = new RadioButton("Brouillon");
        tbBrouillon.setFont(Font.font("Inter",16));

        RadioButton tbEnProduction = new RadioButton("En Production");
        tbEnProduction.setFont(Font.font("Inter",16));

        this.tgEtat.getToggles().addAll(tbBrouillon,tbEnProduction);
        if (etat.equals("0")) {
            this.tgEtat.selectToggle(tbBrouillon);
        }
        else if (etat.equals("N")) {
            this.tgEtat.selectToggle(tbEnProduction);
        }

        Label lTexteIntroductif = new Label("Texte introductif :");
        lTexteIntroductif.setFont(Font.font("Inter",16));
        lTexteIntroductif.setTextFill(Color.BLACK);

        this.taTexteIntroductif = new TextArea(texteIntroductif);
        this.taTexteIntroductif.setMaxSize(500,100);

        this.btImageIntroductif = new Button("Choisir une image");

        if (imageSelectIntroductif == null) {
            this.imageIntroduction = new Image("file:img/NoImage.png");
        }
        else {
            this.imageIntroduction = imageSelectIntroductif;
        }
        this.miniatureIntroduction = new ImageView(this.imageIntroduction);
        this.miniatureIntroduction.setFitHeight(100);
        this.miniatureIntroduction.setPreserveRatio(true);

        Label lTexteSucces = new Label("Texte en cas de succès :");
        lTexteSucces.setFont(Font.font("Inter",16));
        lTexteSucces.setTextFill(Color.BLACK);

        this.taTexteSucces = new TextArea(texteSucces);
        this.taTexteSucces.setMaxSize(500,100);

        this.btImageSucces = new Button("Choisir une image");

        if (imageSelectSucces == null) {
            this.imageSucces = new Image("file:img/NoImage.png");
        }
        else {
            this.imageSucces = imageSelectSucces;
        }
        this.miniatureSucces = new ImageView(this.imageSucces);
        this.miniatureSucces.setFitHeight(100);
        this.miniatureSucces.setPreserveRatio(true);


        Label lTexteEchec = new Label("Texte en cas d'échec :");
        lTexteEchec.setFont(Font.font("Inter",16));
        lTexteEchec.setTextFill(Color.BLACK);

        this.taTexteEchec = new TextArea(texteEchec);
        this.taTexteEchec.setMaxSize(500,100);

        this.btImageEchec = new Button("Choisir une image");

        if (imageSelectEchec == null) {
            this.imageEchec = new Image("file:img/NoImage.png");
        }
        else {
            this.imageEchec = imageSelectEchec;
        }
        this.miniatureEchec = new ImageView(this.imageEchec);
        this.miniatureEchec.setFitHeight(100);
        this.miniatureEchec.setPreserveRatio(true);

        this.btAssocierTileSet = new Button("Associer avec un Tileset >");



        GridPane centerCenterCenter = new GridPane();
        ColumnConstraints col0 = new ColumnConstraints();
        col0.setPercentWidth(20);
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(40);
        centerCenterCenter.setPadding(new Insets(8));
        centerCenterCenter.setHgap(8);
        centerCenterCenter.setVgap(8);
        centerCenterCenter.add(lNomCarte,0,0);
        centerCenterCenter.add(this.tfNomCarte,1,0,5,1);
        centerCenterCenter.add(lEtat,0,1);
        centerCenterCenter.add(tbBrouillon,1,1);
        centerCenterCenter.add(tbEnProduction,1,2);

        centerCenterCenter.add(lTexteIntroductif,0,3);
        centerCenterCenter.add(this.taTexteIntroductif,1,3);
        centerCenterCenter.add(this.miniatureIntroduction,2,3);
        centerCenterCenter.add(this.btImageIntroductif,3,3);
        centerCenterCenter.add(lTexteSucces,0,4);
        centerCenterCenter.add(this.taTexteSucces,1,4);
        centerCenterCenter.add(this.miniatureSucces,2,4);
        centerCenterCenter.add(this.btImageSucces,3,4);
        centerCenterCenter.add(lTexteEchec,0,5);
        centerCenterCenter.add(this.taTexteEchec,1,5);
        centerCenterCenter.add(this.miniatureEchec,2,5);
        centerCenterCenter.add(this.btImageEchec,3,5);
        centerCenterCenter.add(this.btAssocierTileSet,0,6);

        BorderPane centerCenter = new BorderPane();
        centerCenter.setPadding(new Insets(8));
        centerCenter.setCenter(centerCenterCenter);


        BorderPane center = new BorderPane();
        center.setStyle("-fx-background-color: #76F8E0;");
        center.setTop(topCenter);
        center.setCenter(centerCenter);
        return center;
    }

    /**
     * Constructeur
     * @param control
     * @param pseudo
     * @param nomCarte
     * @param etat
     * @param texteIntroductif
     * @param texteSucces
     * @param texteEchec
     * @param imageSelectIntroductif
     * @param imageSelectSucces
     * @param imageSelectEchec
     */
    public ConceptModifierCarte(ControlleurScene control, String pseudo ,String nomCarte, String etat,
                                String texteIntroductif, String texteSucces, String texteEchec,
                                Image imageSelectIntroductif, Image imageSelectSucces, Image imageSelectEchec){

        this.control = control;

        FlowPane top = new FlowPane();
        top.setPadding(new Insets(8));
        top.setStyle("-fx-background-color: #1FAFAF");
        top.setAlignment(Pos.CENTER_LEFT);
        Label lTop = new Label("Modifier une carte");
        lTop.setFont(Font.font("Inter", FontWeight.BOLD,32));
        lTop.setTextFill(Color.BLACK);
        top.getChildren().add(lTop);
        this.setTop(top);


        this.setCenter(this.center(pseudo,nomCarte, etat,
                        texteIntroductif, texteSucces, texteEchec,
                        imageSelectIntroductif, imageSelectSucces, imageSelectEchec));


        FlowPane leftBottom = new FlowPane();
        leftBottom.setPadding(new Insets(8));
        leftBottom.setAlignment((Pos.CENTER_LEFT));
        this.lRetour = new Label("Retour");
        lRetour.setOnMouseClicked(new LesLabels(this.control,"BDcartes" ));
        this.lRetour.setUnderline(true);
        this.lRetour.setFont(Font.font("Inter", FontWeight.BOLD,16));
        this.lRetour.setTextFill(Color.web("87009D"));
        leftBottom.getChildren().add(this.lRetour);

        FlowPane centerBottom = new FlowPane();
        centerBottom.setPadding(new Insets(8));
        centerBottom.setAlignment((Pos.CENTER_LEFT));
        this.btSauvegarder = new Button("Sauvegarder");
        this.btSauvegarder.setFont(Font.font("Inter",32));
        centerBottom.getChildren().add(this.btSauvegarder);

        BorderPane bottom = new BorderPane();
        bottom.setPadding(new Insets(8));
        bottom.setStyle("-fx-background-color: #76F8E0");
        bottom.setLeft(leftBottom);
        bottom.setCenter(centerBottom);

        this.setBottom(bottom);

        this.sceneConceptModifCarte = new Scene(this, 1050, 700);
    }

    public Scene getSceneConceptModifCarte(){
        return this.sceneConceptModifCarte;
    }
}