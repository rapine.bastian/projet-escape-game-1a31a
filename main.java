import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class main extends Application {

    private ControlleurScene control;


    @Override
    public void start(Stage stage) throws Exception {
        this.control = new ControlleurScene(stage);
        Scene sc = this.control.getSceneConnecter();
        stage.setScene(sc);
        stage.show();
    }


}
