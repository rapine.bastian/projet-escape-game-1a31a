import java.sql.*;

public class Stat  {

    private boolean actif;
    private int numPartie;
    private String scenario;
    private String temps;
    private boolean fini;
    private String pseudo;
    private boolean afini;

    public Stat(String pseudo, boolean actif, int numPartie, String scenario, String temps, boolean finir){
        this.pseudo = pseudo;
        this.actif = actif;
        this.numPartie = numPartie;
        this.scenario = scenario;
        this.temps = temps;
        this.afini = finir;
        if (finir){
            String fini = "Réussie";
        }else {
            String fini =  "echec";}
    }


    public String getPseudo(){
        return this.pseudo;
    }

    public boolean getActif(){
        return this.actif;
    }

    public int getNumPartie(){
        return this.numPartie;
    }

    public String getScenario(){
        return this.scenario;
    }

    public String getTemps(){
        return this.temps;
    }

    public boolean getAfini(){
        return this.afini;
    }



}
