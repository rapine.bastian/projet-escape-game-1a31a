import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ConceptCreationScenario extends BorderPane {


    /**
     * Label clickable permettant le retour à la scene Concepteur
     */
    private final Label lRetour;

    /**
     * Label clickable permettant d'aller à la scene de creation de carte
     */
    private Label lCreerCarte;

    /**
     * Label clickable permettant d'aller à la scene de creation d'enigme
     */
    private Label lCreerEnigme;

    /**
     * Label affichant le nom de l'utilisateur connecte
     */
    private Label lPseudoConcept;


    /**
     * TextField donnant le nom du scenario
     */
    private TextField tfNomScenario;

    /**
     * TextArea donnant le texte introductif
     */
    private TextArea taTexteIntroductif;

    /**
     * TextArea donnant le texte en cas de succes
     */
    private TextArea taTexteSucces;

    /**
     * TextArea donnant le texte en cas d'echec
     */
    private TextArea taTexteEchec;

    /**
     * Button ouvrant un FileChooser afin de selectionner une image
     */
    private Button btImage;

    /**
     * Image illustrant l'énigme
     */
    private Image image;

    /**
     * ImageView permettant d'affiche l'image
     */
    private ImageView miniature;

    /**
     * Button permettant d'ajouter une carte à un scenario
     */
    private Button btAjouterCarte;

    /**
     * Table montrant les cartes du scenario
     */
    private TableView table;

    /**
     * Groupe de bouton permettant de savoir si la carte est prete ou non
     */
    private ToggleGroup tgEtat;

    /**
     * Button de creer le scenario
     */
    private Button btCreer;

    private Scene sceneConceptCreationScenar;
    private ControlleurScene control;

    /**
     * Permet de creer la partie centrale (voir constructeur)
     * @param pseudo
     * @return un BorderPane a mettre dans la partie centrale
     */
    private BorderPane setCenter(String pseudo){



        Label lCreerScenario = new Label("Créer scénario");
        lCreerScenario.setOnMouseClicked(new LesLabels(this.control,"Créer scénario" ));
        lCreerScenario.setFont(Font.font("Inter",16));
        lCreerScenario.setTextFill(Color.web("EC00F1"));

        this.lCreerCarte = new Label("Créer carte");
        lCreerCarte.setOnMouseClicked(new LesLabels(this.control,"Créer carte" ));
        this.lCreerCarte.setFont(Font.font("Inter",16));
        this.lCreerCarte.setTextFill(Color.web("87009D"));
        this.lCreerCarte.setUnderline(true);

        this.lCreerEnigme = new Label("Créer énigme");
        lCreerEnigme.setOnMouseClicked(new LesLabels(this.control,"Créer énigme" ));
        this.lCreerEnigme.setFont(Font.font("Inter",16));
        this.lCreerEnigme.setTextFill(Color.web("87009D"));
        this.lCreerEnigme.setUnderline(true);

        FlowPane leftTopCenter = new FlowPane();
        leftTopCenter.setAlignment(Pos.CENTER_LEFT);
        leftTopCenter.setPadding(new Insets(8));
        leftTopCenter.setHgap(8);
        leftTopCenter.setVgap(8);
        leftTopCenter.getChildren().addAll(lCreerScenario,this.lCreerCarte,this.lCreerEnigme);


        Label lPseudo = new Label("Pseudo : ");
        lPseudo.setFont(Font.font("Inter",16));
        lPseudo.setTextFill(Color.BLACK);

        this.lPseudoConcept = new Label(pseudo);
        lPseudoConcept.setFont(Font.font("Inter", FontWeight.BOLD,16));
        lPseudoConcept.setTextFill(Color.BLACK);

        HBox centerTopCenter = new HBox();
        centerTopCenter.setAlignment(Pos.CENTER_RIGHT);
        centerTopCenter.setPadding(new Insets(8));
        centerTopCenter.getChildren().addAll(lPseudo,this.lPseudoConcept);


        BorderPane topCenter = new BorderPane();
        topCenter.setPadding(new Insets(8));
        topCenter.setLeft(leftTopCenter);
        topCenter.setCenter(centerTopCenter);



        Label lNomScenario = new Label("Nom du scénario :");
        lNomScenario.setFont(Font.font("Inter",16));
        lNomScenario.setTextFill(Color.BLACK);

        this.tfNomScenario = new TextField();

        Label lTexteIntroductif = new Label("Texte introductif :");
        lTexteIntroductif.setFont(Font.font("Inter",16));
        lTexteIntroductif.setTextFill(Color.BLACK);

        this.taTexteIntroductif = new TextArea();
        this.taTexteIntroductif.setMaxSize(500,100);

        Label lTexteSucces = new Label("Texte en cas de succès :");
        lTexteSucces.setFont(Font.font("Inter",16));
        lTexteSucces.setTextFill(Color.BLACK);

        this.taTexteSucces = new TextArea();
        this.taTexteSucces.setMaxSize(500,100);

        Label lTexteEchec = new Label("Texte en cas d'échec :");
        lTexteEchec.setFont(Font.font("Inter",16));
        lTexteEchec.setTextFill(Color.BLACK);

        this.taTexteEchec = new TextArea();
        this.taTexteEchec.setMaxSize(500,100);


        GridPane centerCenterCenter = new GridPane();
        centerCenterCenter.setPadding(new Insets(8));
        centerCenterCenter.setHgap(8);
        centerCenterCenter.setVgap(8);
        centerCenterCenter.add(lNomScenario,0,0);
        centerCenterCenter.add(this.tfNomScenario,1,0,5,1);
        centerCenterCenter.add(lTexteIntroductif,0,1);
        centerCenterCenter.add(this.taTexteIntroductif,1,1,5,1);
        centerCenterCenter.add(lTexteSucces,0,2);
        centerCenterCenter.add(this.taTexteSucces,1,2,5,1);
        centerCenterCenter.add(lTexteEchec,0,3);
        centerCenterCenter.add(this.taTexteEchec,1,3,5,1);


        BorderPane centerCenter = new BorderPane();
        centerCenter.setPadding(new Insets(8));
        centerCenter.setCenter(centerCenterCenter);



        this.btAjouterCarte = new Button("Ajouter une carte >");

        FlowPane topBottomCenter = new FlowPane();
        topBottomCenter.setPadding(new Insets(8));
        topBottomCenter.getChildren().add(this.btAjouterCarte);
        topBottomCenter.setAlignment(Pos.CENTER_LEFT);

        //A IMPLEMENTER
        this.table = new TableView();
        TableColumn<String, String> columnNum = new TableColumn<>("N°");
        columnNum.setMinWidth(50);
        TableColumn<String, String> columnNomCarte = new TableColumn<>("Nom Carte");
        columnNomCarte.setMinWidth(150);
        TableColumn<String, String> columnEtat = new TableColumn<>("Etat");
        columnEtat.setMinWidth(50);
        TableColumn<String, String> columnDelete = new TableColumn<>();
        columnDelete.setMinWidth(50);

        this.table.getColumns().addAll(columnNum,columnNomCarte,columnEtat,columnDelete);
        this.table.setStyle("-fx-background-color: #76F8E0");
        this.table.setMaxHeight(200);
        this.table.setPlaceholder(new Label("Il n'y a aucune carte"));

        FlowPane leftBottomCenter = new FlowPane();
        leftBottomCenter.getChildren().add(this.table);

        Label lEtat = new Label("Etat :");
        lEtat.setFont(Font.font("Inter",16));

        this.tgEtat = new ToggleGroup();
        RadioButton tbBrouillon = new RadioButton("Brouillon");
        tbBrouillon.setFont(Font.font("Inter",16));

        RadioButton tbEnProduction = new RadioButton("En Production");
        tbEnProduction.setFont(Font.font("Inter",16));

        this.tgEtat.getToggles().addAll(tbBrouillon,tbEnProduction);
        this.tgEtat.selectToggle(tbBrouillon);

        this.btCreer = new Button("Créer");
        this.btCreer.setFont(Font.font("Inter",32));


        GridPane rightBottomCenter = new GridPane();
        rightBottomCenter.setPadding(new Insets(8));
        rightBottomCenter.setHgap(8);
        rightBottomCenter.setVgap(8);
        rightBottomCenter.setAlignment(Pos.CENTER_LEFT);
        rightBottomCenter.add(lEtat,0,0);
        rightBottomCenter.add(tbBrouillon,1,0);
        rightBottomCenter.add(tbEnProduction,1,1);
        rightBottomCenter.add(btCreer,0,2,2,1);


        this.btImage = new Button("Choisir une image");

        this.image = new Image("file:img/NoImage.png");
        this.miniature = new ImageView(this.image);
        this.miniature.setFitHeight(100);
        this.miniature.setPreserveRatio(true);

        VBox centerBottomCenter = new VBox();
        centerBottomCenter.setPadding(new Insets(8));
        centerBottomCenter.setAlignment(Pos.CENTER);
        centerBottomCenter.setSpacing(8);
        centerBottomCenter.getChildren().addAll(this.miniature,this.btImage);



        BorderPane bottomCenter = new BorderPane();
        bottomCenter.setPadding(new Insets(8));
        bottomCenter.setTop(topBottomCenter);
        bottomCenter.setLeft(leftBottomCenter);
        bottomCenter.setRight(rightBottomCenter);
        bottomCenter.setCenter(centerBottomCenter);




        BorderPane center = new BorderPane();
        center.setStyle("-fx-background-color: #76F8E0;");
        center.setTop(topCenter);
        center.setCenter(centerCenter);
        center.setBottom(bottomCenter);
        return center;
    }


    public ConceptCreationScenario(ControlleurScene control, String pseudo){
        this.control = control;

        FlowPane top = new FlowPane();
        top.setPadding(new Insets(8));
        top.setStyle("-fx-background-color: #1FAFAF");
        top.setAlignment(Pos.CENTER_LEFT);
        Label lTop = new Label("Créer un scénario");
        lTop.setFont(Font.font("Inter", FontWeight.BOLD,32));
        lTop.setTextFill(Color.BLACK);
        top.getChildren().add(lTop);
        this.setTop(top);


        this.setCenter(this.setCenter(pseudo));


        FlowPane bottom = new FlowPane();
        bottom.setPadding(new Insets(8));
        bottom.setStyle("-fx-background-color: #76F8E0");
        bottom.setAlignment((Pos.CENTER_LEFT));
        this.lRetour = new Label("Retour");
        lRetour.setOnMouseClicked(new LesLabels(this.control,"menuConcept" ));
        this.lRetour.setUnderline(true);
        this.lRetour.setFont(Font.font("Inter", FontWeight.BOLD,16));
        this.lRetour.setTextFill(Color.web("87009D"));
        bottom.getChildren().add(this.lRetour);
        this.setBottom(bottom);

        this.sceneConceptCreationScenar = new Scene(this, 1050, 700);
    }

    public Scene getSceneConceptCreationScenar(){
        return this.sceneConceptCreationScenar;
    }
}