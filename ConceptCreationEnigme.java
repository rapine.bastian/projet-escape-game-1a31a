import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ConceptCreationEnigme extends BorderPane {



    /**
     * Label clickable permettant le retour à la scene Concepteur
     */
    private final Label lRetour;

    /**
     * Label clickable permettant d'aller à la scene de creation de scenario
     */
    private Label lCreerScenario;

    /**
     * Label clickable permettant d'aller à la scene de creation de carte
     */
    private Label lCreerCarte;

    /**
     * Label affichant le nom de l'utilisateur connecte
     */
    private Label lPseudoConcept;


    /**
     * TextField donnant le nom de l'enigme
     */
    private TextField tfNomEnigme;

    /**
     * TextArea donnant le texte de la question
     */
    private TextArea taTexteQuestion;

    /**
     * TextArea donnant le texte de la reponse
     */
    private TextArea taTexteReponse;

    /**
     * TextArea donnant le texte de l'indice
     */
    private TextArea taTexteIndice;

    /**
     * Button ouvrant un FileChooser afin de selectionner une image pour l'enigme
     */
    private Button btImage;

    /**
     * Image illustrant l'énigme
     */
    private Image image;

    /**
     * ImageView permettant d'affiche l'image
     */
    private ImageView miniature;

    /**
     * Button permettant de creer l'enigme
     */
    private final Button btCreer;

    private Scene sceneConceptCreationEnigme;
    private ControlleurScene control;


    /**
     * Permet de creer la partie centrale (voir constructeur)
     * @param pseudo
     * @return BorderPane a mettre dans la partie centrale
     */
    private BorderPane center(String pseudo){



        this.lCreerScenario = new Label("Créer scénario");
        lCreerScenario.setOnMouseClicked(new LesLabels(this.control,"Créer scénario" ));
        this.lCreerScenario.setFont(Font.font("Inter",16));
        this.lCreerScenario.setTextFill(Color.web("87009D"));
        this.lCreerScenario.setUnderline(true);

        this.lCreerCarte = new Label("Créer carte");
        lCreerCarte.setOnMouseClicked(new LesLabels(this.control,"Créer carte" ));
        this.lCreerCarte.setFont(Font.font("Inter",16));
        this.lCreerCarte.setTextFill(Color.web("87009D"));
        this.lCreerCarte.setUnderline(true);

        Label lCreerEnigme = new Label("Créer énigme");
        lCreerEnigme.setOnMouseClicked(new LesLabels(this.control,"Créer énigmes" ));
        lCreerEnigme.setFont(Font.font("Inter",16));
        lCreerEnigme.setTextFill(Color.web("EC00F1"));


        FlowPane leftTopCenter = new FlowPane();
        leftTopCenter.setAlignment(Pos.CENTER_LEFT);
        leftTopCenter.setPadding(new Insets(8));
        leftTopCenter.setHgap(8);
        leftTopCenter.setVgap(8);
        leftTopCenter.getChildren().addAll(this.lCreerScenario,this.lCreerCarte,lCreerEnigme);

        Label lPseudo = new Label("Pseudo : ");
        lPseudo.setFont(Font.font("Inter", FontWeight.BOLD,16));
        lPseudo.setTextFill(Color.BLACK);

        this.lPseudoConcept = new Label(pseudo);
        lPseudoConcept.setFont(Font.font("Inter",16));
        lPseudoConcept.setTextFill(Color.BLACK);

        HBox centerTopCenter = new HBox();
        centerTopCenter.setAlignment(Pos.CENTER_RIGHT);
        centerTopCenter.setPadding(new Insets(8));
        centerTopCenter.getChildren().addAll(lPseudo,this.lPseudoConcept);

        BorderPane topCenter = new BorderPane();
        topCenter.setPadding(new Insets(8));
        topCenter.setLeft(leftTopCenter);
        topCenter.setCenter(centerTopCenter);



        Label lNomEnigme = new Label("Nom de l'énigme :");
        lNomEnigme.setFont(Font.font("Inter", FontWeight.BOLD,16));
        lNomEnigme.setTextFill(Color.BLACK);

        this.tfNomEnigme = new TextField();

        Label lTexteQuestion = new Label("La Question :");
        lTexteQuestion.setFont(Font.font("Inter", FontWeight.BOLD,16));
        lTexteQuestion.setTextFill(Color.BLACK);
        lTexteQuestion.setUnderline(true);

        this.taTexteQuestion = new TextArea();
        this.taTexteQuestion.setMaxSize(500,100);


        Label lTexteReponse = new Label("La Réponse :");
        lTexteReponse.setFont(Font.font("Inter", FontWeight.BOLD,16));
        lTexteReponse.setTextFill(Color.BLACK);
        lTexteReponse.setUnderline(true);

        this.taTexteReponse = new TextArea();
        this.taTexteReponse.setMaxSize(500,100);

        Label lTexteIndice = new Label("Indice(s) :");
        lTexteIndice.setFont(Font.font("Inter", FontWeight.BOLD,16));
        lTexteIndice.setTextFill(Color.BLACK);
        lTexteIndice.setUnderline(true);

        this.taTexteIndice = new TextArea();
        this.taTexteIndice.setMaxSize(500,100);

        this.btImage = new Button("Choisir une image");

        this.image = new Image("file:img/NoImage.png");
        this.miniature = new ImageView(this.image);
        this.miniature.setFitHeight(100);
        this.miniature.setPreserveRatio(true);

        GridPane centerCenterCenter = new GridPane();
        centerCenterCenter.setPadding(new Insets(8));
        centerCenterCenter.setHgap(8);
        centerCenterCenter.setVgap(8);
        centerCenterCenter.add(lNomEnigme,0,0);
        centerCenterCenter.add(this.tfNomEnigme,1,0);
        centerCenterCenter.add(lTexteQuestion,0,2,2,1);
        centerCenterCenter.add(this.taTexteQuestion,0,3,2,1);
        centerCenterCenter.add(lTexteReponse,2,2,2,1);
        centerCenterCenter.add(this.taTexteReponse,2,3,2,1);
        centerCenterCenter.add(lTexteIndice,0,4,2,1);
        centerCenterCenter.add(this.taTexteIndice,0,5,2,1);
        centerCenterCenter.add(this.btImage,2,5);
        centerCenterCenter.add(this.miniature,3,5);

        BorderPane centerCenter = new BorderPane();
        centerCenter.setPadding(new Insets(8));
        centerCenter.setCenter(centerCenterCenter);


        BorderPane center = new BorderPane();
        center.setStyle("-fx-background-color: #76F8E0;");
        center.setTop(topCenter);
        center.setCenter(centerCenter);
        return center;
    }


    public ConceptCreationEnigme(ControlleurScene control, String pseudo){
        this.control = control;

        FlowPane top = new FlowPane();
        top.setPadding(new Insets(8));
        top.setStyle("-fx-background-color: #1FAFAF");
        top.setAlignment(Pos.CENTER_LEFT);
        Label lTop = new Label("Créer une énigme");
        lTop.setFont(Font.font("Inter", FontWeight.BOLD,32));
        lTop.setTextFill(Color.BLACK);
        top.getChildren().add(lTop);
        this.setTop(top);


        this.setCenter(this.center(pseudo));


        FlowPane leftBottom = new FlowPane();
        leftBottom.setPadding(new Insets(8));
        leftBottom.setAlignment((Pos.CENTER_LEFT));
        this.lRetour = new Label("Retour");
        lRetour.setOnMouseClicked(new LesLabels(this.control,"menuConcept" ));
        this.lRetour.setUnderline(true);
        this.lRetour.setFont(Font.font("Inter", FontWeight.BOLD,16));
        this.lRetour.setTextFill(Color.web("87009D"));
        leftBottom.getChildren().add(this.lRetour);

        FlowPane centerBottom = new FlowPane();
        centerBottom.setPadding(new Insets(8));
        centerBottom.setAlignment((Pos.CENTER_LEFT));
        this.btCreer = new Button("Créer");
        this.btCreer.setFont(Font.font("Inter",32));
        centerBottom.getChildren().add(this.btCreer);

        BorderPane bottom = new BorderPane();
        bottom.setPadding(new Insets(8));
        bottom.setStyle("-fx-background-color: #76F8E0");
        bottom.setLeft(leftBottom);
        bottom.setCenter(centerBottom);

        this.setBottom(bottom);

        this.sceneConceptCreationEnigme = new Scene(this, 1050, 700);
    }

    public Scene getSceneConceptCreationEnigme(){
        return this.sceneConceptCreationEnigme;
    }
}