import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.sql.SQLException;

public class LesLabels implements EventHandler<MouseEvent> {

    private ControlleurScene control;
    private String nomScene, pseudo;

    public LesLabels(ControlleurScene control, String nomScene){
        this.control = control;
        this.nomScene = nomScene;
    }

    public LesLabels(ControlleurScene control, String nomScene, String pseudo){
        this.control = control;
        this.nomScene = nomScene;
        this.pseudo = pseudo;
    }

    @Override
    public void handle(MouseEvent e){
        if (this.nomScene.equals("BDscenarios")){
            this.control.changerScene(this.control.getBdScenario().getBdScenarioScene());
        }
        if (this.nomScene.equals("BDcartes")){
            System.out.println(123123);
            System.out.println(this.control);
            this.control.changerScene(this.control.getBdCarte().getBdCarteScene());
            }
        if (this.nomScene.equals("BDenigmes")){
            this.control.changerScene(this.control.getBdEnigme().getBdEnigmeScene());
        }

        if (this.nomScene.equals("se déconnecter ?")){
            this.control.changerScene(this.control.getSceneConnecter());
        }

        if (this.nomScene.equals("menuConcept")){
            System.out.println(this.control);
            this.control.changerScene(this.control.getMenuConcepteur().getSceneMenuConcepteur());
        }

        if (this.nomScene.equals("Créer scénario")){
            this.control.changerScene(this.control.getConceptCreationScenario().getSceneConceptCreationScenar());
        }
        if (this.nomScene.equals("Créer carte")){
            this.control.changerScene(this.control.getConceptCreationCarte().getSceneConceptCreationCarte());
        }
        if (this.nomScene.equals("Créer énigmes")){
            this.control.changerScene(this.control.getConceptCreationEnigme().getSceneConceptCreationEnigme());
        }

        if (this.nomScene.equals("AdminFicheJoueur")){
            try {
                this.control.setAdminFicheJoueur(new AdminFicheJoueur(this.control, this.control.getAdminRechercher().getTfNomJoueur().getText()));
                this.control.changerScene(this.control.getAdminFicheJoueur().getScnenAdminFicheJoueur());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }

    }
}
