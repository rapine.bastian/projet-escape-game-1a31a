import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.util.List;

public class Bd_scenarios {

    private TableView table;
    private Scene bdScenarioScene;
    private ControlleurScene control;



    public Bd_scenarios(ControlleurScene control) throws SQLException {
        this.control = control;
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(15,20,10,10));
        root.setTop(creerHeader());
        root.setBottom(creerFooter());
        root.setCenter(creerFormulaire());
        this.bdScenarioScene = new Scene(root, 1200, 600);
    }

    public Scene getBdScenarioScene(){
        return this.bdScenarioScene;
    }

    public GridPane creerFormulaire() throws SQLException {

        GridPane grid = new GridPane();
        grid.setVgap(15);

        Label titreCarte = new Label("Liste Scenario");
        titreCarte.setUnderline(true);
        titreCarte.setFont(new Font("Jaldi", 15));
        titreCarte.setStyle("-fx-font-weight: bold");

        Label lRechercher = new Label("Rechercher :");
        lRechercher.setFont(new Font("Jaldi", 17));
        lRechercher.setStyle("-fx-font-weight: bold");
        TextField tPseudo = new TextField();
        tPseudo.setPrefWidth(200);
        tPseudo.setPrefHeight(25);


        ObservableList<ContenuTableScenario> data = FXCollections.observableArrayList();
        List<Scenario> carteList = this.control.getScenarioJDBC().getScenario();

        for (Scenario c : carteList){
            data.add(new ContenuTableScenario(this.control,c.getScenario(), c.getEtat()));
        }

        this.table = new TableView();
        TableColumn<ContenuTableScenario, String> col0 = new TableColumn<>("Nom scénario");
        col0.setCellValueFactory(new PropertyValueFactory<ContenuTableScenario, String>("attribut0"));

        TableColumn<ContenuTableScenario, String> col1 = new TableColumn<>("Etat");
        col1.setCellValueFactory(new PropertyValueFactory<ContenuTableScenario, String>("attribut1"));

        TableColumn<ContenuTableScenario, String> col2 = new TableColumn<>("");
        col2.setCellValueFactory(new PropertyValueFactory<ContenuTableScenario, String>("modifier"));
        this.table.getColumns().addAll(col0, col1, col2);
        this.table.setItems(data);


        this.table.setStyle("-fx-background-color: #42AD87");
        this.table.setMaxHeight(200);
        this.table.setPlaceholder(new Label("Il n'y a aucune enigmes"));

        grid.getColumnConstraints().add(new ColumnConstraints(130));
        grid.getColumnConstraints().add(new ColumnConstraints(0));
        grid.getColumnConstraints().add(new ColumnConstraints(650));
        grid.getColumnConstraints().add(new ColumnConstraints(160));

        grid.add(titreCarte, 0, 1);
        grid.add(lRechercher, 3,1);
        grid.add(tPseudo, 4,1);
        grid.add(table, 2,4);

        grid.setGridLinesVisible(false);
        grid.setAlignment(Pos.TOP_LEFT);

        grid.setMargin(lRechercher, new Insets(0,5,0,20));
        grid.setMargin(titreCarte, new Insets(0,5,0,20));
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setStyle("-fx-background-color: #76F8E0;");
        grid.setHgap(2);
        return grid;
    }

    public BorderPane creerFooter() {

        BorderPane grid = new BorderPane();


        Label Retour = new Label("Retour");
        Retour.setOnMouseClicked(new LesLabels(this.control,"menuConcept" ));

        Retour.setFont(new Font("Saira", 20));
        Retour.setTextFill(Color.PURPLE);
        Retour.setStyle("-fx-font-weight: bold");
        Retour.setUnderline(true);

        grid.setMargin(Retour, new Insets(0,0,20,15));
        grid.setLeft(Retour);
        grid.setStyle("-fx-background-color: #76F8E0;");

        return grid;
    }

    public GridPane creerHeader() {

        GridPane grid = new GridPane();

        ImageView img = new ImageView("./user.png");
        img.setFitWidth(20);
        img.setPreserveRatio(true);

        ImageView img1 = new ImageView("./home.png");
        img1.setFitWidth(40);
        img1.setPreserveRatio(true);


        Label titre = new Label("Consulter la base de données ");
        Label BDscenario = new Label("BD scénarios ");

        BDscenario.setOnMouseClicked(new LesLabels(this.control, "BDscenarios" ));
        Label BDcartes = new Label("BD cartes ");
        BDcartes.setOnMouseClicked(new LesLabels(this.control, "BDcartes"));
        Label BDénigmes = new Label("BD énigmes ");
        BDénigmes.setOnMouseClicked(new LesLabels(this.control,"BDenigmes" ));

        Label Pseudo = new Label("Pseudo :" );
        Label Exemple = new Label (" Exemple");

        grid.add(img1,1,0);
        grid.add(titre, 2,0);
        grid.add(BDscenario, 1,2);
        grid.add(BDénigmes, 3,2);
        grid.add(BDcartes, 2,2);
        grid.add(img,4,2);
        grid.add(Pseudo, 5,2);
        grid.add(Exemple, 6,2);

        Pseudo.setStyle("-fx-font-weight: bold");
        Pseudo.setFont(new Font(18));

        BDscenario.setFont(new Font("Saira", 15));
        BDcartes.setFont(new Font("Saira", 15));
        BDénigmes.setFont(new Font("Saira", 15));

        BDscenario.setStyle("-fx-font-weight: bold");
        BDcartes.setStyle("-fx-font-weight: bold");
        BDénigmes.setStyle("-fx-font-weight: bold");

        BDscenario.setTextFill(Color.FUCHSIA);
        BDcartes.setTextFill(Color.PURPLE);
        BDénigmes.setTextFill(Color.PURPLE);

        titre.setFont(new Font("Jaldi", 30));
        titre.setStyle("-fx-font-weight: bold");

        Pseudo.setStyle("-fx-font-weight: bold");

        grid.setMargin(titre, new Insets(30,0,20,0));
        grid.setAlignment(Pos.CENTER_LEFT);
        grid.setStyle("-fx-background-color: #1FAFAF;");

        return grid;
    }

}

