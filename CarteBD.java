import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarteBD {

    private ConnectionMySQL laConnexion;

    public CarteBD(ConnectionMySQL laconnexion){
        this.laConnexion = laconnexion;
    }

    public List<Carte> getCarte() throws SQLException{
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select nomca, brouillonca, messageTxt  from Carte natural join TEXTE");
        List<Carte> res = new ArrayList<>();
        while (rs.next()){
            String nomca = rs.getString(1);
            boolean etat = rs.getString(2).charAt(0) == 'O'; // true si c'est un brouillon
            String intro = rs.getString(3);
            rs.next();
            String succes = rs.getString(3);
            rs.next();
            String echec = rs.getString(3);
            res.add(new Carte(nomca, etat,intro, succes, echec));
        }
        return res;
    }

    public Carte getCarteParNom(String nom) throws SQLException{
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select nomca, brouillonca, messageTxt  from Carte natural join TEXTE where nomCa = '" + nom+"'");
        if (rs.next()){
            String nomca = rs.getString(1);
            boolean etat = rs.getString(2).charAt(0) == 'O'; // true si c'est un brouillon
            String intro = rs.getString(3);
            rs.next();
            String succes = rs.getString(3);
            rs.next();
            String echec = rs.getString(3);
            return new Carte(nomca, etat,intro, succes, echec);

        }
        else{
            throw new SQLException("Cet enigme n'existe pas.");
        }
    }
/*
    public void insererCarte( Carte c, byte[] imgSucces, byte[] imgEchec, byte[] imgIntro) throws SQLException{

        PreparedStatement pst = this.laConnexion.prepareStatement("insert into Carte values(?,?,?,?,?,?,?)");`
        ResultSet rs = pst.executeQuery("select IFNULL(max(idca),0) lemax from Carte");
        rs.next();
        int res = rs.getInt("lemax"); rs.close();
        pst.setInt(1, res+1);
        pst.setString(2, c.getNom());
        pst.getString(3, c.getPlan());
        pst.getBoolean(4, c.getEtat());
        pst.getInt(5, c.getNumConcepteur()); // peut etre changer car on peut avoir le num ailleurs
        pst.getInt(6, c.getTs());
        pst.executeUpdate();

        PreparedStatement pst2 = this.laConnexion.prepareStatement("insert into TEXTE values(?,?,?,?,?,?)"); //insertion texte intro
        ResultSet rs2 = st.executeQuery("select IFNULL(max(idtxt),0) lemax from TEXTE");
        rs2.next();
        int res2 = rs2.getInt("lemax"); rs2.close();
        pst2.setInt(1, res2 +1);
        pst2.setString(2, c.getMessageTxtIntro());
        pst2.setString(3, c.getImgTxtIntro());
        Blob b2=laConnexion.createBlob (); //image
        b2.setBytes(1, imgIntro);
        pst2.setBlob(4, b2);
        pst2.setInt(5, res+1);
        pst2.setInt(6, 1);

        PreparedStatement pst3 = this.laConnexion.prepareStatement("insert into TEXTE values(?,?,?,?,?,?)"); //insertion texte en cas de succés
        pst3.setInt(1, res2 +2);
        pst3.setString(2, c.getMessageTxtSucces());
        pst3.setString(3, c.getImgTxtSucces());
        Blob b3=laConnexion.createBlob (); //image
        b3.setBytes(1, imgSucces);
        pst3.setBlob(4, b3);
        pst3.setInt(5, res+1);
        pst3.setInt(6, 2);

        PreparedStatement pst4 = this.laConnexion.prepareStatement("insert into TEXTE values(?,?,?,?,?,?)"); //insertion texte en cas d'échec
        pst4.setInt(1, res2 +3);
        pst4.setString(2, c.getMessageTxtEchec());
        pst4.setString(3, c.getImgTxtEchec());
        Blob b4=laConnexion.createBlob (); //image
        b4.setBytes(1, imgEchec);
        pst4.setBlob(4, b4);
        pst4.setInt(5, res+1);
        pst4.setInt(6, 3);

 */
}