import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class AdministrateurBD{

    private ConnectionMySQL laconnexion;


    AdministrateurBD(ConnectionMySQL laconnexion){
        this.laconnexion = laconnexion;
    }

    public boolean rechercherJoueurParPseudo(String pseudo)throws SQLException{
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select * from UTILISATEUR where pseudoUt = " + pseudo);
        if (rs.next()){
            return true;
        }
        else{
            return false;
        }
    }

    public List<Stat> getStat(String nomJoueur) throws SQLException{
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select pseudoUt , activeUt, idpa, gagne , tpsresolution from UTILISATEUR natural join PARTIE where pseudoUt ='" + nomJoueur + "'");
        List<Stat> res = new ArrayList<>();
        if (rs.next()){
            while (rs.next()){
                String pseudo = rs.getString(1);
                boolean actif = rs.getString(2).charAt(0) == 'O';
                int numPartie = rs.getInt(3);
                //String scenario = rs.getString(4);
                String temps = rs.getString(5);
                boolean fini = rs.getString(4).charAt(0) == 'O';
                res.add(new Stat(pseudo, actif,numPartie,"scenar",temps,fini));
                rs.next();
            }
        }
        else{
            throw new SQLException("Joueur " + nomJoueur + " n'existe pas.");
        }
        return res;
    }
}
