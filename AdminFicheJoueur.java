import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.sql.*;
import java.sql.SQLException;
import java.util.List;

public class AdminFicheJoueur extends BorderPane {



    /**
     * Label clickable permettant le retour à la scene AdminRechercher
     */
    private Label lRetour;

    /**
     * Label affichant le nom du joueur recherché
     */
    private Label lNomJoueur;

    /**
     * Groupe de bouton permettant de savoir si le joueur est activé ou non
     */
    private ToggleGroup tgActiver;

    /**
     * Table montrant les statistiques des parties du joueur
     */
    private TableView table;

    /**
     * Scene de la classe
     */
    private Scene scnenAdminFicheJoueur;
    private ControlleurScene control;
    private boolean activer;

    /**
     * Permet de creer la partie centrale (voir constructeur)
     * @param nomJoueur
     * @param activer
     * @return un BorderPane a mettre dans la partie centrale
     */


    private BorderPane center(String nomJoueur, boolean activer) throws SQLException {


        this.lRetour = new Label("Retour");
        lRetour.setOnMouseClicked(new LesLabels(this.control,"AdminRechercher" ));


        this.lRetour.setFont(Font.font("Inter",16));
        this.lRetour.setTextFill(Color.web("87009D"));
        this.lRetour.setUnderline(true);

        Label lFicheJoueur = new Label("Fiche du joueur recherché");
        lFicheJoueur.setFont(Font.font("Inter",16));
        lFicheJoueur.setTextFill(Color.BLACK);

        BorderPane topCenter = new BorderPane();
        topCenter.setPadding(new Insets(8));
        topCenter.setBorder(new Border(new BorderStroke(Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,
                            BorderStrokeStyle.NONE,BorderStrokeStyle.NONE,BorderStrokeStyle.SOLID,BorderStrokeStyle.NONE,
                            CornerRadii.EMPTY,BorderWidths.DEFAULT,new Insets(8))));


        FlowPane topTopCenter = new FlowPane();
        topCenter.setTop(topTopCenter);
        topTopCenter.setHgap(8);
        topTopCenter.setAlignment(Pos.TOP_LEFT);
        topTopCenter.getChildren().add(this.lRetour);

        FlowPane bottomTopCenter = new FlowPane();
        topCenter.setBottom(bottomTopCenter);
        bottomTopCenter.setVgap(8);
        bottomTopCenter.setAlignment(Pos.BOTTOM_RIGHT);
        bottomTopCenter.getChildren().add(lFicheJoueur);



        Label lPseudo = new Label("Pseudo :");
        lPseudo.setFont(Font.font("Inter",16));
        lPseudo.setUnderline(true);

        this.lNomJoueur = new Label(nomJoueur);
        lNomJoueur.setFont(Font.font("Inter",16));

        Label lActiver = new Label("Activer le joueur :");
        lActiver.setFont(Font.font("Inter",16));
        lActiver.setUnderline(true);

        this.tgActiver = new ToggleGroup();
        ToggleButton tbOn = new ToggleButton("On");
        tbOn.setFont(Font.font("Inter",16));

        ToggleButton tbOff = new ToggleButton("Off");
        tbOff.setFont(Font.font("Inter",16));

        this.tgActiver.getToggles().addAll(tbOn,tbOff);
        if (activer){
            this.tgActiver.selectToggle(tbOn);
        }
        else {
            this.tgActiver.selectToggle(tbOff);
        }

        Label lStatistiques = new Label("Ses statistiques :");
        lStatistiques.setFont(Font.font("Inter",16));
        lStatistiques.setUnderline(true);





        ObservableList<ContenuTableStat> data = FXCollections.observableArrayList();

        List<Stat> listeStat = this.control.getAdministrateurBD().getStat(pseudo);

        for (Stat s : listeStat){
            data.add(new ContenuTableStat(s.getNumPartie(), s.getScenario(), s.getTemps(), s.getAfini()));
            this.activer = s.getActif();
        }

        this.table = new TableView();


        TableColumn<ContenuTableStat, Integer> col0 = new TableColumn<>("N°Partie");
        col0.setCellValueFactory(new PropertyValueFactory<ContenuTableStat, Integer>("attribut0"));

        TableColumn<ContenuTableStat, String> col1 = new TableColumn<>("Nom Scénario");
        col1.setCellValueFactory(new PropertyValueFactory<ContenuTableStat, String>("attribut1"));

        TableColumn<ContenuTableStat, Date> col2 = new TableColumn<>("Temps joué");
        col2.setCellValueFactory(new PropertyValueFactory<ContenuTableStat, Date>("attribut2"));

        TableColumn<ContenuTableStat, String> col3 = new TableColumn<>("Fini ?");
        col3.setCellValueFactory(new PropertyValueFactory<ContenuTableStat, String>("attribut3"));

        this.table.getColumns().addAll(col0, col1, col2, col3);
        this.table.setItems(data);



        GridPane centerCenter = new GridPane();
        centerCenter.setAlignment(Pos.CENTER);
        centerCenter.setPadding(new Insets(8));
        centerCenter.setHgap(8);
        centerCenter.setVgap(8);

        centerCenter.add(lPseudo,0,0);
        centerCenter.add(this.lNomJoueur,1,0,2,1);
        centerCenter.add(lActiver,0,1);
        centerCenter.add(tbOn,1,1);
        centerCenter.add(tbOff,1,2);
        centerCenter.add(lStatistiques,0,3);
        centerCenter.add(this.table,0,4,2,1);


        BorderPane center = new BorderPane();
        center.setStyle("-fx-background-color: #F3EA9A;");
        center.setTop(topCenter);
        center.setCenter(centerCenter);
        return center;
    }

    /**
     *  Constructeur
     */
    private String pseudo;
    public AdminFicheJoueur(ControlleurScene control, String nomJoueur) throws SQLException {
        this.control = control;
        FlowPane top = new FlowPane();
        top.setPadding(new Insets(8));
        top.setStyle("-fx-background-color: #E40909;");
        top.setAlignment(Pos.CENTER);
        Label lAdminTop = new Label("ADMINISTRATEUR");
        lAdminTop.setFont(Font.font("Inter", FontWeight.BOLD,36));
        lAdminTop.setTextFill(Color.BLACK);
        top.getChildren().add(lAdminTop);
        this.setTop(top);
        this.pseudo = nomJoueur;
        this.setCenter(this.center(nomJoueur, activer));

        this.scnenAdminFicheJoueur = new Scene(this, 1050 ,700);

    }

    public Scene getScnenAdminFicheJoueur(){
        return this.scnenAdminFicheJoueur;
    }
}
