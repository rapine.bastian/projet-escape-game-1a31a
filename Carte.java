public class Carte{

    private String carte;
    private boolean etat;
    private String aide, question, rep;

    public Carte(String carte, boolean etat, String question, String rep, String aide){
        this.carte = carte;
        this.etat = etat;
        this.aide = aide;
        this.rep = rep;
        this.question = question;
    }

    public String getCarte(){ return this.carte; }

    public boolean getEtat(){ return this.etat; }

    public String getAide(){
        return this.aide;
    }

    public String getQuestion(){
        return this.question;
    }

    public String getRep(){
        return this.rep;
    }
}
