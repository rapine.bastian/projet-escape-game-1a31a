import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.SQLException;

public class ControlleurScene {
    private Se_connecter se_connecter;
    private Creer_compte creer_compte;
    private Stage st;
    private ConnectionMySQL connecSQL;
    private ConnexionBD connexionBD;
    private Menu_concepteur menu_concepteur;
    private ConceptCreationCarte conceptCreationCarte;
    private ConceptCreationScenario conceptCreationScenario;
    private AdminFicheJoueur adminFicheJoueur;
    private AdminRechercher adminRechercher;
    private Bd_enigme bdEnigme;
    private EnigmeBD enigmeJDBC;
    private CarteBD carteBD;
    private Bd_scenarios bdScenario;
    private Consulter_bd_Carte bdCarte;
    private AdministrateurBD administrateurBD;
    private ConceptCreationEnigme conceptCreationEnigme;
    private ScenarioBD scenarioJDBC;


    public ControlleurScene(Stage st) throws ClassNotFoundException, SQLException {
        this.st = st;
        this.connecSQL = new ConnectionMySQL();
        this.connecSQL.connecter();
        this.connexionBD = new ConnexionBD(this.connecSQL);
        this.se_connecter = new Se_connecter(this);
        this.creer_compte = new Creer_compte(this);


    }

    public void creerSceneUneFoisConnecteConcepteur() throws SQLException {
        String pseudo = connexionBD.getPseudo();
        this.menu_concepteur = new Menu_concepteur(this, pseudo);
        this.conceptCreationScenario = new ConceptCreationScenario(this, pseudo);
        this.conceptCreationEnigme = new ConceptCreationEnigme(this, pseudo);
        this.conceptCreationCarte = new ConceptCreationCarte(this, pseudo);
        this.enigmeJDBC = new EnigmeBD(this.connecSQL);
        this.carteBD = new CarteBD(this.connecSQL);
        this.scenarioJDBC= new ScenarioBD(this.connecSQL);
        this.bdEnigme = new Bd_enigme(this);
        this.bdScenario = new Bd_scenarios(this);
        this.bdCarte = new Consulter_bd_Carte(this);

    }

    public void creerSceneUneFoisConnecteAdmin() throws SQLException {
        String pseudo = connexionBD.getPseudo();
        this.adminRechercher = new AdminRechercher(this, pseudo);
        this.administrateurBD = new AdministrateurBD(this.connecSQL);

    }


    public AdministrateurBD getAdministrateurBD(){return this.administrateurBD;}
    public AdminFicheJoueur getAdminFicheJoueur(){return this.adminFicheJoueur; }
    public void setAdminFicheJoueur(AdminFicheJoueur ad){this.adminFicheJoueur = ad;}

    public AdminRechercher getAdminRechercher(){
        return this.adminRechercher;
    }

    public Bd_enigme getBdEnigme() {
        return bdEnigme;
    }
    public Bd_scenarios getBdScenario(){return this.bdScenario;}
    public Consulter_bd_Carte getBdCarte(){return this.bdCarte;}



    public EnigmeBD getEnigmeJDBC(){return enigmeJDBC;}
    public CarteBD getCarteJDBC(){return this.carteBD;}
    public ScenarioBD getScenarioJDBC(){return this.scenarioJDBC;}

    public ConceptCreationCarte getConceptCreationCarte(){
        return this.conceptCreationCarte;
    }
    public ConceptCreationEnigme getConceptCreationEnigme(){return  this.conceptCreationEnigme;}
    public ConceptCreationScenario getConceptCreationScenario(){
        return this.conceptCreationScenario;
    }



    public Scene getSceneConnecter(){
        return this.se_connecter.getSceneConnecter();
    }
    public Se_connecter getSeConnecter(){ return this.se_connecter; }

    public Creer_compte getCreerCompte(){
        return this.creer_compte;
    }
    public Menu_concepteur getMenuConcepteur(){
        return this.menu_concepteur;
    }


    public Scene getSceneCreerCompte(){
        return this.creer_compte.getSceneCreerCompteCreerCompte();
    }

    public void changerScene(Scene sc){
        st.setScene(sc);
   }

    public ConnectionMySQL getConnectionMySQL(){return this.connecSQL;}

    public ConnexionBD getConnexionBD(){return this.connexionBD;}

}
