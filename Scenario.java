public class Scenario {

    private String scenario;
    private boolean etat;
    private String aide, question, rep;

    public Scenario(String scenario, boolean etat, String question, String rep, String aide){
        this.scenario = scenario;
        this.etat = etat;
        this.aide = aide;
        this.rep = rep;
        this.question = question;
    }

    public String getScenario(){
        return this.scenario;
    }

    public boolean getEtat(){
        return this.etat;
    }

    public String getAide(){
        return this.aide;
    }

    public String getQuestion(){
        return this.question;
    }

    public String getRep(){
        return this.rep;
    }
}
