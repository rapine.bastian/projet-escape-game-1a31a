public class Enigme {

    private String enigme;
    private boolean etat;
    private String aide, question, rep;

    public Enigme(String enigme, boolean etat, String question, String rep, String aide){
        this.enigme = enigme;
        this.etat = etat;
        this.aide = aide;
        this.rep = rep;
        this.question = question;
    }

    public String getEnigme(){
        return this.enigme;
    }

    public boolean getEtat(){
        return this.etat;
    }

    public String getAide(){
        return this.aide;
    }

    public String getQuestion(){
        return this.question;
    }

    public String getRep(){
        return this.rep;
    }
}
