import javafx.scene.control.Button;

import java.sql.Date;

public class ContenuTableStat {

    private int attribut0;
    private String attribut1;
    private String attribut2;
    private String attribut3;

    public ContenuTableStat(int attribut0, String attribut1, String attribut2, boolean attribut3 ) {

        this.attribut0 = attribut0;
        this.attribut1 = attribut1;
        this.attribut2 = attribut2;
        if (attribut3){
            this.attribut3 = "brouillon";
        } else {
            this.attribut1 = "en production";
        }

    }

    public int getAttribut0() {
        return this.attribut0;
    }

    public String getAttribut1() {
        return this.attribut1;
    }

    public String getAttribut2(){return this.attribut2;}
    public  String getAttribut3(){return  this.attribut3;}

}
