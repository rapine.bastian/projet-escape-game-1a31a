import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.InputStream;

public class Menu_concepteur {

    private ControlleurScene control;
    private Scene sceneMenuConcepteur;
    private ConnexionBD connexionBD;
    private String pseudo;

    public Menu_concepteur(ControlleurScene control, String pseudo) {
        this.control = control;
        this.pseudo = pseudo;
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(15,20,10,10));
        root.setTop(creerHeader());
        root.setBottom(creerFooter());
        root.setCenter(creerFormulaire());
        this.sceneMenuConcepteur = new Scene(root, 1050, 700);
    }

    public Scene getSceneMenuConcepteur() {
        return sceneMenuConcepteur;
    }


    public GridPane creerHeader() {

        GridPane grid = new GridPane();
        // Image
        ImageView img = new ImageView("./user.png");
        img.setFitWidth(20);
        img.setPreserveRatio(true);

        Label titre = new Label("CONCEPTEUR");
        Label Pseudo = new Label("Pseudo :" );
        Label Exemple = new Label (this.pseudo);

        grid.add(titre, 2,0);
        grid.add(img, 6,3);
        grid.add(Pseudo, 7,3);
        grid.add(Exemple, 8,3);

        titre.setFont(new Font("Jaldi", 30));
        titre.setStyle("-fx-font-weight: bold");
        titre.setTextFill(Color.BLACK);
        Pseudo.setTextFill(Color.BLACK);
        Exemple.setTextFill(Color.BLACK);

        Pseudo.setStyle("-fx-font-weight: bold");
        Pseudo.setFont(new Font(18));
        Exemple.setFont(new Font( 14));

        grid.setMargin(titre, new Insets(15,0,10,0));
        grid.setAlignment(Pos.CENTER);
        grid.setStyle("-fx-background-color: #1FAFAF;");
        grid.setVgap(5);
        grid.setHgap(10);

        return grid;
    }
    public BorderPane creerFooter() {

        BorderPane grid = new BorderPane();


        Label numFiche = new Label("Se deconnecter ?");
        numFiche.setOnMouseClicked(new LesLabels(this.control,"se déconnecter ?" ));


        numFiche.setFont(new Font("Roboto", 16));
        numFiche.setUnderline(true);
        numFiche.setStyle("-fx-font-weight: bold");
        grid.setCenter(numFiche);
        grid.setStyle("-fx-background-color: #4FFDDE;");

        return grid;
    }

    public GridPane creerFormulaire() {

        GridPane grid = new GridPane();
        grid.setVgap(10);

        VBox tableau = new VBox();

        Label titreCarte = new Label("Tableau de bord");
        titreCarte.setUnderline(true);
        titreCarte.setFont(new Font("Jaldi", 18));
        titreCarte.setStyle("-fx-font-weight: bold");

        DropShadow shadow = new DropShadow();

        Image img = new Image("./Base_de_donnees.png");
        ImageView view = new ImageView(img);
        view.setFitHeight(50);
        view.setPreserveRatio(true);

        Image img1 = new Image("./Base_de_donnees.png");
        ImageView view1 = new ImageView(img);
        view1.setFitHeight(50);
        view1.setPreserveRatio(true);

        Image img2 = new Image("./Base_de_donnees.png");
        ImageView view2 = new ImageView(img);
        view2.setFitHeight(50);
        view2.setPreserveRatio(true);

        Image img3 = new Image("./settings-gears.png");
        ImageView view3 = new ImageView(img3);
        view3.setFitHeight(50);
        view3.setPreserveRatio(true);

        Image img4 = new Image("./settings-gears.png");
        ImageView view4 = new ImageView(img4);
        view4.setFitHeight(50);
        view4.setPreserveRatio(true);

        Image img5 = new Image("./settings-gears.png");
        ImageView view5 = new ImageView(img5);
        view5.setFitHeight(50);
        view5.setPreserveRatio(true);


        Button Scenario_modif = new Button("BDScenario") ;
        Scenario_modif.setOnAction(new LesBoutons(this.control));
        Scenario_modif.setGraphic(view);
        Button Carte_modif = new Button("BDCarte") ;
        Carte_modif.setOnAction(new LesBoutons(this.control));
        Carte_modif.setGraphic(view1);
        Button Enigme_modif = new Button("BDEnigme") ;
        Enigme_modif.setOnAction(new LesBoutons(this.control));
        Enigme_modif.setGraphic(view2);

        Button Scenario_crea = new Button("Scenario") ;
        Scenario_crea.setOnAction(new LesBoutons(this.control));
        Scenario_crea.setGraphic(view3);



        Button Carte_crea = new Button("Carte") ;
        Carte_crea.setOnAction(new LesBoutons(this.control));
        Carte_crea.setGraphic(view4);
        Button Enigme_crea = new Button("Enigme");
        Enigme_crea.setOnAction(new LesBoutons(this.control));
        Enigme_crea.setGraphic(view5);

        Text modification = new Text("Modification \n ( Consulter la BD )");
        Text creation = new Text("Création");

        modification.setFont(new Font("Tahoma", 22));
        creation.setFont(new Font("Tahoma",22));

        modification.setTextAlignment(TextAlignment.CENTER);
        creation.setTextAlignment(TextAlignment.CENTER);

        Scenario_modif.setEffect(shadow);
        Carte_modif.setEffect(shadow);
        Enigme_modif.setEffect(shadow);
        Scenario_crea.setEffect(shadow);
        Carte_crea.setEffect(shadow);
        Enigme_crea.setEffect(shadow);

        Scenario_modif.setPrefWidth(175);
        Scenario_modif.setPrefHeight(75);

        Carte_modif.setPrefWidth(175);
        Carte_modif.setPrefHeight(75);

        Enigme_modif.setPrefWidth(175);
        Enigme_modif.setPrefHeight(75);

        Scenario_crea.setPrefWidth(175);
        Scenario_crea.setPrefHeight(75);

        Carte_crea.setPrefWidth(175);
        Carte_crea.setPrefHeight(75);

        Enigme_crea.setPrefWidth(175);
        Enigme_crea.setPrefHeight(75);

        grid.add(titreCarte, 3, 2);
        grid.add(Scenario_modif,3, 4);
        grid.add(Scenario_crea,5 ,4);
        grid.add(Carte_modif,2, 5);
        grid.add(Enigme_modif,3, 5);
        grid.add(Carte_crea,5, 5);
        grid.add(Enigme_crea,6, 5);

        grid.add(modification,3,7);
        grid.add(creation,5,7);

        grid.setMargin(titreCarte, new Insets(0,5,0,20));
        grid.setStyle("-fx-background-color: #4FFDDE;");
        grid.setVgap(10);
        grid.setHgap(25);

        return grid;
    }
}
