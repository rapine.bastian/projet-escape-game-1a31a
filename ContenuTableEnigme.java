import javafx.scene.control.Button;

public class ContenuTableEnigme {

    private String attribut0;
    private String attribut1;
    private Button modifier;
    private ControlleurScene control;

    public ContenuTableEnigme(ControlleurScene control, String attribut0, boolean attribut1) {
        this.control = control;
        this.attribut0 = attribut0;
        if (attribut1){
            this.attribut1 = "brouillon";
        } else {
            this.attribut1 = "en production";
        }
        this.modifier = new Button("Modifier");
        this.modifier.setOnAction(new ControlleurModifier(this.control, this));
    }

    public String getAttribut0() {
        return this.attribut0;
    }

    public String getAttribut1() {
        return this.attribut1;
    }

    public Button getModifier(){
        return this.modifier;
    }
}
