import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.geometry.Pos;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class AdminRechercher extends BorderPane {


    /**
     * Label affichant le nom de l'utilisateur connecté
     */
    private Label lPseudoAdmin;

    /**
     * TextField donnant le nom du joueur recherché
     */
    private TextField tfNomJoueur;

    /**
     * Label clickable permettant la déconnection
     */
    private final Label lSeDeconnecter;

    /**
     * Permet de creer la partie centrale (voir constructeur)
     * @param pseudo
     * @return un BorderPane a mettre dans la partie centrale
     */

    private ControlleurScene control;
    private Scene sceneAdminRechercher;

    private BorderPane center(String pseudo){
        Label lPseudoTop = new Label("Pseudo :");
        lPseudoTop.setFont(Font.font("Inter", FontWeight.BOLD,24));
        lPseudoTop.setTextFill(Color.BLACK);

        this.lPseudoAdmin = new Label(pseudo);
        lPseudoAdmin.setFont(Font.font("Inter",24));
        lPseudoAdmin.setTextFill(Color.BLACK);

        Label lRecherche = new Label("Recherche du joueur");
        lRecherche.setFont(Font.font("Inter",32));
        lRecherche.setTextFill(Color.BLACK);

        Label lPseudoCenter = new Label("Pseudo :");
        lPseudoCenter.setFont(Font.font("Inter", FontWeight.BOLD,24));
        lPseudoCenter.setTextFill(Color.BLACK);
        this.tfNomJoueur = new TextField();

        Label rechercher = new Label("rechercher");
        rechercher.setOnMouseClicked(new LesLabels(this.control, "AdminFicheJoueur", this.getTfNomJoueur().getText()));

        BorderPane center = new BorderPane();
        center.setPadding(new Insets(8));
        center.setStyle("-fx-background-color: #F3EA9A;");

        FlowPane topCenter = new FlowPane();
        topCenter.setPadding(new Insets(8));
        topCenter.setHgap(8);
        topCenter.setVgap(8);
        topCenter.setAlignment(Pos.TOP_RIGHT);
        topCenter.getChildren().addAll(lPseudoTop,this.lPseudoAdmin);
        center.setTop(topCenter);

        BorderPane centerCenter = new BorderPane();
        centerCenter.setPadding(new Insets(8));
        center.setCenter(centerCenter);

        FlowPane topCenterCenter = new FlowPane();
        topCenterCenter.setPadding(new Insets(8));
        topCenterCenter.setAlignment(Pos.CENTER);
        topCenterCenter.getChildren().add(lRecherche);
        centerCenter.setTop(topCenterCenter);

        FlowPane centerCenterCenter = new FlowPane();
        centerCenterCenter.setPadding(new Insets(8));
        centerCenterCenter.setHgap(8);
        centerCenterCenter.setVgap(8);
        centerCenterCenter.setAlignment(Pos.CENTER);
        centerCenterCenter.getChildren().addAll(lPseudoCenter,this.tfNomJoueur, rechercher);
        centerCenter.setCenter(centerCenterCenter);

        return center;
    }

    /**
     * Constructeur
     * @param pseudo
     * @param control
     */
    public AdminRechercher(ControlleurScene control, String pseudo){
        this.control = control;

        FlowPane top = new FlowPane();
        top.setPadding(new Insets(8));
        top.setStyle("-fx-background-color: #E40909;");
        top.setAlignment(Pos.CENTER);
        Label lAdminTop = new Label("ADMINISTRATEUR");
        lAdminTop.setFont(Font.font("Inter", FontWeight.BOLD,36));
        lAdminTop.setTextFill(Color.BLACK);
        top.getChildren().add(lAdminTop);
        top.setMinHeight(50);
        this.setTop(top);

        this.setCenter(this.center(pseudo));

        FlowPane bottom = new FlowPane();
        bottom.setPadding(new Insets(8));
        bottom.setStyle("-fx-background-color: #F3EA9A;");
        bottom.setAlignment((Pos.CENTER));
        this.lSeDeconnecter = new Label("se déconnecter ?");
        lSeDeconnecter.setOnMouseClicked(new LesLabels(this.control,"se déconnecter ?" ));
        this.lSeDeconnecter.setUnderline(true);
        this.lSeDeconnecter.setFont(Font.font("Inter", FontWeight.BOLD,16));
        this.lSeDeconnecter.setTextFill(Color.web("87009D"));
        bottom.getChildren().add(this.lSeDeconnecter);
        this.setBottom(bottom);
        this.sceneAdminRechercher = new Scene(this, 1050, 700);

    }

    public Scene getSceneAdminRechercher(){
        return this.sceneAdminRechercher;
    }
    public TextField getTfNomJoueur(){return this.tfNomJoueur;}
}
