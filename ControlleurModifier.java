import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import java.sql.SQLException;

public class ControlleurModifier implements EventHandler<ActionEvent> {

    private ContenuTableEnigme contenuTableEnigme;
    private ControlleurScene control;
    private ContenuTableCarte contenuTableCarte;
    private ContenuTableScenario contenuTableScenario;



    public ControlleurModifier(ControlleurScene control, ContenuTableEnigme contenuTableEnigme) {
        this.control = control;
        this.contenuTableEnigme = contenuTableEnigme;
    }

    public ControlleurModifier(ControlleurScene control, ContenuTableCarte contenuTableCarte) {
        this.control = control;
        this.contenuTableCarte = contenuTableCarte;
    }

    public ControlleurModifier(ControlleurScene control, ContenuTableScenario contenuTableScenario) {
        this.control = control;
        this.contenuTableScenario = contenuTableScenario;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Button b = (Button) actionEvent.getTarget();
        if (contenuTableEnigme != null) {
            try {
                Enigme enigme = this.control.getEnigmeJDBC().getEnigmeParNom(this.contenuTableEnigme.getAttribut0());
                ConceptModifierEnigme conceptModifierEnigme = new ConceptModifierEnigme(this.control, this.control.getConnexionBD().getPseudo(), enigme.getEnigme(), enigme.getQuestion(), enigme.getRep(), enigme.getAide());
                control.changerScene(conceptModifierEnigme.getSceneConceptModifEnigme());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        if (contenuTableCarte != null) {
            try {
                String etatCarte;
                Carte carte = this.control.getCarteJDBC().getCarteParNom(this.contenuTableCarte.getAttribut0());
                if (carte.getEtat() == true){
                    etatCarte = "brouillon";
                } else { etatCarte = "en Production";}
                ConceptModifierCarte conceptModifierCarte = new ConceptModifierCarte(this.control, this.control.getConnexionBD().getPseudo(), carte.getCarte(), etatCarte, carte.getQuestion(), carte.getRep(), carte.getAide(), null, null, null);
                control.changerScene(conceptModifierCarte.getSceneConceptModifCarte());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        if (contenuTableScenario != null) {
            try {
                String etatCarte;
                Scenario scenario = this.control.getScenarioJDBC().getScenarioParNom(this.contenuTableScenario.getAttribut0());
                if (scenario.getEtat() == true){
                    etatCarte = "brouillon";
                } else { etatCarte = "en Production";}
                ConceptModifierScenario conceptModifierScenario = new ConceptModifierScenario(this.control, this.control.getConnexionBD().getPseudo(), scenario.getScenario(), scenario.getQuestion(), scenario.getRep(), scenario.getAide(), null, etatCarte);
                control.changerScene(conceptModifierScenario.getSceneModifScenar());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
